﻿Public Class ems_login
    '#############Form##############
    Private Sub ems_login_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        MsgBox("程式執行的過程中，請勿開啟任何資料庫，以免造成不預期的情況發生。", MsgBoxStyle.Information, "提醒")
        'ExcelInit()
        oXLBook = oXLApp.Workbooks.Open(CurDir() & "\dat\passwd.xls")
        ExcelRead("\dat\passwd.xls")
    End Sub

    Private Sub ems_login_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        ExcelInit()
    End Sub

    '#####################Object#################
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Access = 0
        For i = 2 To userpass_Cellvar
            If TextBox1.Text = Excel_userpass(i, 0) And TextBox1.Text <> Nothing And TextBox2.Text = Excel_userpass(i, 1) And TextBox2.Text <> Nothing Then
                Data_item.admin = TextBox1.Text
                Access = 1
            End If
        Next
        If Access = 1 Then
            ems_mode.Show()
            InputInit()
            Me.Hide()
        Else
            MsgBox("登入失敗。")
            InputInit()
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBox2.TextChanged
        TextBox2.PasswordChar = "*"
    End Sub

    '#######函數自訂################
    Sub InputInit()
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub
End Class