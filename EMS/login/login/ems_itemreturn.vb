﻿Public Class ems_itemreturn
    '#################################Form################################
    Private Sub ems_itemreturn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ExcelRead("\dat\器材清單.xls")
        ExcelRead("\dat\器材借用紀錄.xls")
        '讀取資料庫中資料，並顯示於區塊中
        For i = 2 To itemreco_Cellvar
            If Excel_itemreco(i, 1) = Data_unit.UnitId And Excel_itemreco(i, 8) = "未歸還" Then
                CheckedListBox1.Items.Add(Excel_itemreco(i, 6))
            End If
        Next
        Data_item.LR_num = 0
        Label2.Text = "總共 " & CheckedListBox1.Items.Count & " 項" & vbNewLine & "已勾選 " & Data_item.LR_num & " 項"
        '避免因為全部勾選，而導致其他程式碼參數index=-1
        CheckedListBox1.SetSelected(0, True)
    End Sub

    Private Sub ems_itemreturn_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        TmpInit(0, 1, 1)
        ems_mode.Show()
    End Sub

    '#############################CheckedListBox##################################
    Private Sub CheckedListBox1_ItemCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        '因為該觸發事件會發生在打勾動作之前，所以CheckedListBox1.CheckedItems.Count的值會是打勾前的值
        '因此要做以下判斷，以求正確的CheckedListBox1.CheckedItems.Count
        If CheckedListBox1.GetItemChecked(CheckedListBox1.SelectedIndex()) = True Then
            Data_item.LR_num = CheckedListBox1.CheckedItems.Count - 1
        Else
            Data_item.LR_num = CheckedListBox1.CheckedItems.Count + 1
        End If
        Label2.Text = "總共 " & CheckedListBox1.Items.Count & " 項" & vbNewLine & "已勾選 " & Data_item.LR_num & " 項"
    End Sub

    '############################Button########################
    Private Sub 歸還_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 歸還.Click
        Dim MsgResult As MsgBoxResult = MsgBox("確定要送出嗎?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, "送出請求")
        If (MsgResult = MsgBoxResult.Yes) Then
            Data_item.LR_num = Data_item.LR_num - 1
            ReDim Data_item.Id(Data_item.LR_num)
            ReDim Data_item.Name(Data_item.LR_num)
            ReDim Data_item.Return_reco(Data_item.LR_num)
            For i = 0 To Data_item.LR_num '陣列索引從零開始算
                Data_item.Name(i) = CheckedListBox1.CheckedItems.Item(i)
            Next
            For j = 0 To Data_item.LR_num
                For i = 2 To itemlist_Cellvar
                    If Data_item.Name(j) = Excel_itemlist(i, 2) Then
                        Data_item.Id(j) = Excel_itemlist(i, 0)
                        Excel_itemlist(i, 4) = Excel_itemlist(i, 4) + 1 '剩餘數量
                    ElseIf Data_item.Name(j) = Excel_itemreco(i, 6) And Data_basic.Id = Excel_itemreco(i, 3) And Excel_itemreco(i, 8) = "未歸還" Then
                        Data_item.Return_reco(j) = i
                    End If
                Next
            Next
            Try
                ExcelWrite("\dat\器材清單.xls")
                ExcelWrite("\dat\器材借用紀錄.xls")
                MsgBox("恭喜器材歸還成功。", , "提醒")
                For i = 0 To Data_item.LR_num
                    CheckedListBox1.Items.Remove(Data_item.Name(i))
                Next
                Label2.Text = "總共 " & CheckedListBox1.Items.Count & " 項" & vbNewLine & "已勾選 0 項"
            Catch
                MsgBox("系統錯誤，器材歸還失敗。", , "提醒")
            End Try
        End If
    End Sub

    Private Sub 全部勾選_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 全部勾選.Click

        Items_allCheck(True)
    End Sub

    Private Sub 重設_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 重設.Click
        Items_allCheck(False)
    End Sub

    '###################自訂函數######################
    Sub Items_allCheck(ByVal boolin As Boolean)
        For i = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetSelected(i,True)
            CheckedListBox1.SetItemChecked(i, boolin)
        Next
    End Sub
End Class