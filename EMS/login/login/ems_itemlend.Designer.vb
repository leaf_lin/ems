﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ems_itemlend
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.確認 = New System.Windows.Forms.Button
        Me.取消 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Clear = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.移除ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.移除全部ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.剩餘數量ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.詳細資料ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("DFKai-SB", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label1.Location = New System.Drawing.Point(104, 44)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(199, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "器材借用清單"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("DFKai-SB", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label2.Location = New System.Drawing.Point(33, 93)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "器材代碼 :"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(141, 92)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(180, 25)
        Me.TextBox1.TabIndex = 2
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("DFKai-SB", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.DateTimePicker1.Location = New System.Drawing.Point(141, 258)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(236, 27)
        Me.DateTimePicker1.TabIndex = 4
        Me.DateTimePicker1.Value = New Date(2016, 5, 10, 0, 0, 0, 0)
        '
        '確認
        '
        Me.確認.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.確認.Location = New System.Drawing.Point(175, 315)
        Me.確認.Margin = New System.Windows.Forms.Padding(4)
        Me.確認.Name = "確認"
        Me.確認.Size = New System.Drawing.Size(100, 29)
        Me.確認.TabIndex = 5
        Me.確認.Text = "確認"
        Me.確認.UseVisualStyleBackColor = True
        '
        '取消
        '
        Me.取消.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.取消.Location = New System.Drawing.Point(283, 315)
        Me.取消.Margin = New System.Windows.Forms.Padding(4)
        Me.取消.Name = "取消"
        Me.取消.Size = New System.Drawing.Size(100, 29)
        Me.取消.TabIndex = 6
        Me.取消.Text = "取消"
        Me.取消.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("DFKai-SB", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(34, 289)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(349, 22)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "*請確認所有資料正確，確認後無誤請按確認鍵"
        '
        'ListBox1
        '
        Me.ListBox1.DisplayMember = "V"
        Me.ListBox1.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 17
        Me.ListBox1.Location = New System.Drawing.Point(31, 125)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(352, 123)
        Me.ListBox1.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("DFKai-SB", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label4.Location = New System.Drawing.Point(30, 263)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 19)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "預計歸還 :"
        '
        'Clear
        '
        Me.Clear.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Clear.Location = New System.Drawing.Point(328, 92)
        Me.Clear.Name = "Clear"
        Me.Clear.Size = New System.Drawing.Size(55, 26)
        Me.Clear.TabIndex = 12
        Me.Clear.Text = "Clear"
        Me.Clear.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.移除ToolStripMenuItem, Me.移除全部ToolStripMenuItem, Me.剩餘數量ToolStripMenuItem, Me.詳細資料ToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(415, 27)
        Me.MenuStrip1.TabIndex = 13
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        '移除ToolStripMenuItem
        '
        Me.移除ToolStripMenuItem.Name = "移除ToolStripMenuItem"
        Me.移除ToolStripMenuItem.Size = New System.Drawing.Size(51, 23)
        Me.移除ToolStripMenuItem.Text = "移除"
        '
        '移除全部ToolStripMenuItem
        '
        Me.移除全部ToolStripMenuItem.Name = "移除全部ToolStripMenuItem"
        Me.移除全部ToolStripMenuItem.Size = New System.Drawing.Size(81, 23)
        Me.移除全部ToolStripMenuItem.Text = "移除全部"
        '
        '剩餘數量ToolStripMenuItem
        '
        Me.剩餘數量ToolStripMenuItem.Name = "剩餘數量ToolStripMenuItem"
        Me.剩餘數量ToolStripMenuItem.Size = New System.Drawing.Size(81, 23)
        Me.剩餘數量ToolStripMenuItem.Text = "剩餘數量"
        '
        '詳細資料ToolStripMenuItem
        '
        Me.詳細資料ToolStripMenuItem.Name = "詳細資料ToolStripMenuItem"
        Me.詳細資料ToolStripMenuItem.Size = New System.Drawing.Size(81, 23)
        Me.詳細資料ToolStripMenuItem.Text = "詳細資料"
        '
        'ems_itemlend
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 355)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Clear)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.取消)
        Me.Controls.Add(Me.確認)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "ems_itemlend"
        Me.Text = "器材借用"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents 確認 As Button
    Friend WithEvents 取消 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clear As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents 移除ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 移除全部ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 剩餘數量ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 詳細資料ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
