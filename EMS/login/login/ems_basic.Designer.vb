﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ems_basic
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.資料修改ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Label6 = New System.Windows.Forms.Label
        Me.NAME_input = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.ID_input = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.PHONE_input = New System.Windows.Forms.TextBox
        Me.確定 = New System.Windows.Forms.Button
        Me.取消 = New System.Windows.Forms.Button
        Me.修改 = New System.Windows.Forms.Button
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox5
        '
        Me.TextBox5.ContextMenuStrip = Me.ContextMenuStrip1
        Me.TextBox5.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(50, 174)
        Me.TextBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox5.Size = New System.Drawing.Size(292, 104)
        Me.TextBox5.TabIndex = 19
        Me.TextBox5.TabStop = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.資料修改ToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(139, 28)
        '
        '資料修改ToolStripMenuItem
        '
        Me.資料修改ToolStripMenuItem.Name = "資料修改ToolStripMenuItem"
        Me.資料修改ToolStripMenuItem.Size = New System.Drawing.Size(138, 24)
        Me.資料修改ToolStripMenuItem.Text = "資料修改"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("DFKai-SB", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label6.Location = New System.Drawing.Point(46, 141)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 19)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "電話 :"
        '
        'NAME_input
        '
        Me.NAME_input.Location = New System.Drawing.Point(122, 97)
        Me.NAME_input.Margin = New System.Windows.Forms.Padding(4)
        Me.NAME_input.Name = "NAME_input"
        Me.NAME_input.Size = New System.Drawing.Size(219, 25)
        Me.NAME_input.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("DFKai-SB", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label4.Location = New System.Drawing.Point(46, 56)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 19)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "學號 :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("DFKai-SB", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(15, 333)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(223, 15)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "*僅供課外活動組器材管理使用"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("DFKai-SB", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label2.Location = New System.Drawing.Point(107, 9)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 30)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "借用人資料"
        '
        'ID_input
        '
        Me.ID_input.Location = New System.Drawing.Point(123, 55)
        Me.ID_input.Margin = New System.Windows.Forms.Padding(4)
        Me.ID_input.Name = "ID_input"
        Me.ID_input.Size = New System.Drawing.Size(219, 25)
        Me.ID_input.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("DFKai-SB", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label1.Location = New System.Drawing.Point(46, 98)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 19)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "姓名 :"
        '
        'PHONE_input
        '
        Me.PHONE_input.Location = New System.Drawing.Point(122, 140)
        Me.PHONE_input.Margin = New System.Windows.Forms.Padding(4)
        Me.PHONE_input.Multiline = True
        Me.PHONE_input.Name = "PHONE_input"
        Me.PHONE_input.Size = New System.Drawing.Size(219, 26)
        Me.PHONE_input.TabIndex = 3
        '
        '確定
        '
        Me.確定.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.確定.Location = New System.Drawing.Point(50, 286)
        Me.確定.Margin = New System.Windows.Forms.Padding(4)
        Me.確定.Name = "確定"
        Me.確定.Size = New System.Drawing.Size(85, 29)
        Me.確定.TabIndex = 4
        Me.確定.Text = "確定"
        Me.確定.UseVisualStyleBackColor = True
        '
        '取消
        '
        Me.取消.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.取消.Location = New System.Drawing.Point(256, 286)
        Me.取消.Margin = New System.Windows.Forms.Padding(4)
        Me.取消.Name = "取消"
        Me.取消.Size = New System.Drawing.Size(85, 29)
        Me.取消.TabIndex = 6
        Me.取消.Text = "取消"
        Me.取消.UseVisualStyleBackColor = True
        '
        '修改
        '
        Me.修改.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.修改.Location = New System.Drawing.Point(153, 286)
        Me.修改.Margin = New System.Windows.Forms.Padding(4)
        Me.修改.Name = "修改"
        Me.修改.Size = New System.Drawing.Size(85, 29)
        Me.修改.TabIndex = 5
        Me.修改.Text = "修改"
        Me.修改.UseVisualStyleBackColor = True
        '
        'ems_basic
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(391, 357)
        Me.Controls.Add(Me.修改)
        Me.Controls.Add(Me.PHONE_input)
        Me.Controls.Add(Me.取消)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.確定)
        Me.Controls.Add(Me.NAME_input)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ID_input)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "ems_basic"
        Me.Text = "借用人資料"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents NAME_input As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ID_input As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents PHONE_input As TextBox
    Friend WithEvents 確定 As Button
    Friend WithEvents 取消 As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 資料修改ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 修改 As System.Windows.Forms.Button
End Class
