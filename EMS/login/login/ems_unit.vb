﻿Public Class ems_unit
    '###########Form################
    Private Sub ems_unit_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        ExcelRead("\dat\單位資料.xls")
        TextBox2.Text = "單位代號 : " & TextBox1.Text
    End Sub

    Private Sub ems_unit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        TmpInit(0, 1, 0)
        ems_mode.Show()
    End Sub

    '#################Object#########################
    Private Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBox1.TextChanged
        Access = 0
        TmpInit(0, 1, 0)
        TextBox2.Text = "單位代號 : " & TextBox1.Text
        For i = 2 To unitdata_Cellvar
            If TextBox1.Text = Excel_unitdata(i, 0) And TextBox1.Text <> Nothing Then
                Access = 1
                TextBox2.Text = "單位代號 : " & Excel_unitdata(i, 0) & vbNewLine & "單位名稱 : " & Excel_unitdata(i, 1)
                Data_unit.UnitId = Excel_unitdata(i, 0)
                Data_unit.Unit = Excel_unitdata(i, 1)
            End If
        Next
    End Sub

    Private Sub 確定_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 確定.Click
        If Access = 1 Then
            ems_unitreco.Show()
        Else
            MsgBox("請輸入正確的單位代碼。", , "提醒")
        End If
    End Sub
End Class