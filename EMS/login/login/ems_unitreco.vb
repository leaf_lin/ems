﻿Public Class ems_unitreco
    Private Sub ems_unitreco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ExcelRead("\dat\器材借用紀錄.xls")
        '讀取資料庫中資料，並顯示於區塊中
        For i = 2 To itemreco_Cellvar
            If Excel_itemreco(i, 1) = Data_unit.UnitId And Excel_itemreco(i, 8) = "未歸還" Then
                TextBox1.Text = ""
                TextBox1.AppendText("借用日期 : " & Excel_itemreco(i, 0) & vbNewLine & "單位代號 : " & Excel_itemreco(i, 1) & vbNewLine & "單位名稱 : " & Excel_itemreco(i, 2) & vbNewLine & "借用人學號 : " & Excel_itemreco(i, 3) & vbNewLine & "借用人 :" & Excel_itemreco(i, 4) & vbNewLine & "器材編號 : " & Excel_itemreco(i, 5) & vbNewLine & "器材名稱 : " & Excel_itemreco(i, 6) & vbNewLine & "預計歸還 : " & Excel_itemreco(i, 7) & vbNewLine & "歸還紀錄 : " & Excel_itemreco(i, 8) & vbNewLine & "負責人 : " & Excel_itemreco(i, 9) & vbNewLine & vbNewLine)
            ElseIf TextBox1.Text = "" Then
                TextBox1.Text = "無未歸還紀錄。"
            End If
        Next
        Label3.Text = "*如果確認社團資料後，請按確認鍵，否則請按叉叉。"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ems_basic.Show()
        ems_unit.Dispose()
        Me.Dispose()
    End Sub
End Class
