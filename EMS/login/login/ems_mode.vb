﻿Public Class ems_mode
    '##################Button click#################
    Private Sub 器材借用_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 器材借用.Click
        Excel_ModeChoose = 1
        ems_unit.Show()
        Me.Hide()
    End Sub

    Private Sub 器材歸還_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 器材歸還.Click
        Excel_ModeChoose = 0
        ems_unit.Show()
        Me.Hide()
    End Sub

    Private Sub 登出_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles 登出.LinkClicked
        ems_login.Show()
        TmpInit(1, 1, 1)
        Me.Dispose()
    End Sub

    '##############Form################
    Private Sub ems_mode_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ems_login.Close()
    End Sub

    Private Sub ems_mode_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        TmpInit(0, 1, 1)
    End Sub
End Class