﻿Public Class ems_basic
    '########################Form########################
    Private Sub ems_basic_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Access = 0
        ExcelRead("\dat\個人資料.xls")
        TextBox5.Text = "學號 : " & ID_input.Text & vbNewLine & "姓名 : " & NAME_input.Text & vbNewLine & "電話 : " & PHONE_input.Text
    End Sub

    Private Sub ems_basic_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        TmpInit(0, 1, 1)
        ems_mode.Show()
    End Sub

    '#######################TextBox#####################################
    Private Sub ID_input_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ID_input.TextChanged
        ReadOnly_func(0)
        Access = 0
        For i = 2 To Excel_Cellvar
            If ID_input.Text = Excel_basicdata(i, 0) And ID_input.Text <> Nothing Then
                Data_basic.Id = Excel_basicdata(i, 0)
                Data_basic.Name = Excel_basicdata(i, 1)
                Data_basic.Phone = Excel_basicdata(i, 2)
                Excel_datachange = i
                Access = 1
            Else
                TextBox5.Text = "學號 : " & ID_input.Text & vbNewLine & "姓名 : " & NAME_input.Text & vbNewLine & "電話 : " & PHONE_input.Text
            End If
        Next
        If Access = 1 Then
            TextBox5.Text = "學號 : " & Data_basic.Id & vbNewLine & "姓名 : " & Data_basic.Name & vbNewLine & "電話 : " & Data_basic.Phone
            ReadOnly_func(1)
        End If
    End Sub

    Private Sub NAME_input_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NAME_input.TextChanged, PHONE_input.TextChanged
        TextBox5.Text = "學號 : " & ID_input.Text & vbNewLine & "姓名 : " & NAME_input.Text & vbNewLine & "電話 : " & PHONE_input.Text
    End Sub

    '##########################Button##############################
    Private Sub 確定_Click(ByVal sender As Object, ByVal e As EventArgs) Handles 確定.Click
        If Access = 1 Then
            Select Case Excel_ModeChoose
                Case 1
                    ems_itemlend.Show()
                Case 0
                    ems_itemreturn.Show()
                Case Else
                    MsgBox("ems_tmp_module中參數ModeChoose錯誤，模式選擇失敗。", , "警告")
            End Select
            Me.Dispose()
        ElseIf ID_input.Text <> Nothing And NAME_input.Text <> Nothing And PHONE_input.Text <> Nothing And Access <> 1 Then
            Data_basic.Id = ID_input.Text
            Data_basic.Name = NAME_input.Text
            Data_basic.Phone = PHONE_input.Text
            ExcelWrite("\dat\個人資料.xls")
            ems_itemlend.Show()
            Me.Dispose()
        Else
            MsgBox("資料未填寫完整。", , "提醒")
        End If
    End Sub

    Private Sub 修改_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 修改.Click
        '避免按修改鍵，而導致隱私洩漏
        If Access = 1 Then
            ReadOnly_func(0)
            ID_input.Text = Data_basic.Id
            NAME_input.Text = Data_basic.Name
            PHONE_input.Text = Data_basic.Phone
        End If
        Access = 0
    End Sub

    Private Sub 取消_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 取消.Click
        ID_input.Text = ""
        NAME_input.Text = ""
        PHONE_input.Text = ""
        TextBox5.Text = ""
    End Sub

    '###############函數自訂################
    Sub ReadOnly_func(ByVal bool As Integer)
        Dim in_bool As Integer
        If bool = 0 Then
            in_bool = bool + 1
        Else
            in_bool = bool - 1
        End If
        NAME_input.ReadOnly = bool
        PHONE_input.ReadOnly = bool
        NAME_input.TabStop = in_bool
        PHONE_input.TabStop = in_bool
    End Sub
End Class