﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ems_mode
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.器材借用 = New System.Windows.Forms.Button
        Me.器材歸還 = New System.Windows.Forms.Button
        Me.登出 = New System.Windows.Forms.LinkLabel
        Me.SuspendLayout()
        '
        '器材借用
        '
        Me.器材借用.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.器材借用.Location = New System.Drawing.Point(63, 42)
        Me.器材借用.Name = "器材借用"
        Me.器材借用.Size = New System.Drawing.Size(100, 34)
        Me.器材借用.TabIndex = 0
        Me.器材借用.Text = "器材借用"
        Me.器材借用.UseVisualStyleBackColor = True
        '
        '器材歸還
        '
        Me.器材歸還.Font = New System.Drawing.Font("DFKai-SB", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.器材歸還.Location = New System.Drawing.Point(63, 97)
        Me.器材歸還.Name = "器材歸還"
        Me.器材歸還.Size = New System.Drawing.Size(100, 34)
        Me.器材歸還.TabIndex = 1
        Me.器材歸還.Text = "器材歸還"
        Me.器材歸還.UseVisualStyleBackColor = True
        '
        '登出
        '
        Me.登出.ActiveLinkColor = System.Drawing.Color.Red
        Me.登出.AutoSize = True
        Me.登出.Font = New System.Drawing.Font("DFKai-SB", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.登出.Location = New System.Drawing.Point(176, 9)
        Me.登出.Name = "登出"
        Me.登出.Size = New System.Drawing.Size(39, 15)
        Me.登出.TabIndex = 2
        Me.登出.TabStop = True
        Me.登出.Text = "登出"
        '
        'ems_mode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(227, 166)
        Me.Controls.Add(Me.登出)
        Me.Controls.Add(Me.器材歸還)
        Me.Controls.Add(Me.器材借用)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "ems_mode"
        Me.Text = "選擇模式"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents 器材借用 As System.Windows.Forms.Button
    Friend WithEvents 器材歸還 As System.Windows.Forms.Button
    Friend WithEvents 登出 As System.Windows.Forms.LinkLabel
End Class
