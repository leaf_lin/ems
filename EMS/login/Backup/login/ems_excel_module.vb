﻿Module ems_excel_module
    Public oXLApp As New Microsoft.Office.Interop.Excel.Application
    Public oXLBook As Microsoft.Office.Interop.Excel.Workbook
    Public oXLSheet As Microsoft.Office.Interop.Excel.Worksheet
    Public Excel_userpass(Excel_Cellvar, 1) As String
    Public Excel_unitdata(Excel_Cellvar, 1) As String
    Public Excel_itemreco(Excel_Cellvar, 9) As String
    Public Excel_basicdata(Excel_Cellvar, 2) As String
    Public Excel_itemlist(Excel_Cellvar, 6) As String
    Public Excel_emptyget As Integer
    Public Excel_datachange As Integer
    Public Const Excel_Cellvar = 100
    Public Excel_ModeChoose As Integer
    Public Access As Integer

    Sub ExcelRead(ByVal excelPath As String)
        oXLBook = GetObject(CurDir() & excelPath)
        oXLSheet = oXLBook.ActiveSheet
        Select Case excelPath
            Case "\dat\passwd.xls"
                For i = 2 To Excel_Cellvar
                    Excel_userpass(i, 0) = oXLSheet.Cells(i, 1).Value '帳號
                    Excel_userpass(i, 1) = oXLSheet.Cells(i, 2).Value '密碼
                Next
            Case "\dat\單位資料.xls"
                For i = 2 To Excel_Cellvar
                    Excel_unitdata(i, 0) = oXLSheet.Cells(i, 2).Value '單位代號
                    Excel_unitdata(i, 1) = oXLSheet.Cells(i, 3).Value '單位名稱
                Next
            Case "\dat\個人資料.xls"
                For i = 2 To Excel_Cellvar
                    Excel_basicdata(i, 0) = oXLSheet.Cells(i, 1).Value '學號
                    Excel_basicdata(i, 1) = oXLSheet.Cells(i, 2).Value '姓名
                    Excel_basicdata(i, 2) = oXLSheet.Cells(i, 3).Value '電話
                Next
            Case "\dat\器材清單.xls"
                For i = 2 To Excel_Cellvar
                    Excel_itemlist(i, 0) = oXLSheet.Cells(i, 1).Value '編號
                    Excel_itemlist(i, 1) = oXLSheet.Cells(i, 2).Value '種類
                    Excel_itemlist(i, 2) = oXLSheet.Cells(i, 3).Value '名稱
                    Excel_itemlist(i, 3) = oXLSheet.Cells(i, 4).Value '總數量
                    Excel_itemlist(i, 4) = oXLSheet.Cells(i, 5).Value '剩餘數量
                    Excel_itemlist(i, 5) = oXLSheet.Cells(i, 6).Value '外借
                    Excel_itemlist(i, 6) = oXLSheet.Cells(i, 7).Value '備註
                Next
            Case "\dat\器材借用紀錄.xls"
                For i = 2 To Excel_Cellvar
                    Excel_itemreco(i, 0) = oXLSheet.Cells(i, 1).Value '借用日期
                    Excel_itemreco(i, 1) = oXLSheet.Cells(i, 2).Value '單位代號
                    Excel_itemreco(i, 2) = oXLSheet.Cells(i, 3).Value '單位名稱
                    Excel_itemreco(i, 3) = oXLSheet.Cells(i, 4).Value '借用人學號
                    Excel_itemreco(i, 4) = oXLSheet.Cells(i, 5).Value '借用人
                    Excel_itemreco(i, 5) = oXLSheet.Cells(i, 6).Value '器材編號
                    Excel_itemreco(i, 6) = oXLSheet.Cells(i, 7).Value '器材名稱
                    Excel_itemreco(i, 7) = oXLSheet.Cells(i, 8).Value '預計歸還
                    Excel_itemreco(i, 8) = oXLSheet.Cells(i, 9).Value '歸還記錄
                    Excel_itemreco(i, 9) = oXLSheet.Cells(i, 10).Value '負責人
                Next
            Case Else
                MsgBox("系統ExcelRead函數錯誤，ems_excel_module讀取資料庫失敗。", , "警告")
        End Select
    End Sub

    Sub ExcelWrite(ByVal excelPath As String)
        oXLBook = GetObject(CurDir() & excelPath)
        oXLSheet = oXLBook.ActiveSheet
        ExcelGetEmpty(excelPath)
        Select Case excelPath
            Case "\dat\個人資料.xls"
                oXLSheet.Cells(Excel_emptyget, 1).Value = Data_basic.Id
                oXLSheet.Cells(Excel_emptyget, 2).Value = Data_basic.Name
                oXLSheet.Cells(Excel_emptyget, 3).Value = Data_basic.Phone
            Case "\dat\器材清單.xls"
                '全部重新寫入，省略判斷寫入項目
                For i = 2 To Excel_Cellvar
                    oXLSheet.Cells(i, 5).Value = Excel_itemlist(i, 4)
                Next
            Case "\dat\器材借用紀錄.xls"
                If Excel_ModeChoose = 1 Then
                    For i = 0 To Data_item.LR_num
                        oXLSheet.Cells(Excel_emptyget, 1).Value = Format(Now(), "D")
                        oXLSheet.Cells(Excel_emptyget, 2).Value = Data_unit.UnitId
                        oXLSheet.Cells(Excel_emptyget, 3).Value = Data_unit.Unit
                        oXLSheet.Cells(Excel_emptyget, 4).Value = Data_basic.Id
                        oXLSheet.Cells(Excel_emptyget, 5).Value = Data_basic.Name
                        oXLSheet.Cells(Excel_emptyget, 6).Value = Data_item.Id(i)
                        oXLSheet.Cells(Excel_emptyget, 7).Value = Data_item.Name(i)
                        oXLSheet.Cells(Excel_emptyget, 8).Value = Data_item.day
                        oXLSheet.Cells(Excel_emptyget, 9).Value = "未歸還"
                        oXLSheet.Cells(Excel_emptyget, 10).Value = Data_item.admin
                        Excel_emptyget = Excel_emptyget + 1
                    Next
                Else
                    For i = 0 To Data_item.LR_num
                        oXLSheet.Cells(Data_item.Return_reco(i), 11).Value = Format(Now(), "D")
                        oXLSheet.Cells(Data_item.Return_reco(i), 12).Value = Data_unit.UnitId
                        oXLSheet.Cells(Data_item.Return_reco(i), 13).Value = Data_unit.Unit
                        oXLSheet.Cells(Data_item.Return_reco(i), 14).Value = Data_basic.Id
                        oXLSheet.Cells(Data_item.Return_reco(i), 15).Value = Data_basic.Name
                        oXLSheet.Cells(Data_item.Return_reco(i), 16).Value = Data_item.Id(i)
                        oXLSheet.Cells(Data_item.Return_reco(i), 17).Value = Data_item.Name(i)
                        oXLSheet.Cells(Data_item.Return_reco(i), 9).Value = "已歸還"
                        oXLSheet.Cells(Data_item.Return_reco(i), 18).Value = Data_item.admin
                    Next
                End If
            Case Else
                MsgBox("系統ExcelWrite函數錯誤，ems_excel_module寫入資料庫失敗。", , "警告")
        End Select
        oXLSheet = Nothing             'disconnect from the Worksheet
        oXLBook.Close(SaveChanges:=True)    'Save (and disconnect from) the Workbook
    End Sub

    Sub ExcelGetEmpty(ByVal excelPath As String)
        Select Case excelPath
            Case "\dat\個人資料.xls"
                For i = 2 To Excel_Cellvar
                    If Excel_basicdata(i, 0) = "" And Excel_basicdata(i, 1) = "" And Excel_basicdata(i, 2) = "" Then
                        Excel_emptyget = i
                        Exit For 'break
                    End If
                Next
            Case "\dat\器材清單.xls"
            Case "\dat\器材借用紀錄.xls"
                For i = 2 To Excel_Cellvar
                    If Excel_itemreco(i, 0) = "" And Excel_itemreco(i, 1) = "" And Excel_itemreco(i, 2) = "" And Excel_itemreco(i, 3) = "" And Excel_itemreco(i, 4) = "" And Excel_itemreco(i, 5) = "" And Excel_itemreco(i, 6) = "" And Excel_itemreco(i, 7) = "" And Excel_itemreco(i, 8) = "" And Excel_itemreco(i, 9) = "" Then
                        Excel_emptyget = i
                        Exit For 'break
                    End If
                Next
            Case Else
                MsgBox("系統ExcelGetEmpty函數錯誤，ems_excel_module分析資料庫失敗。", , "警告")
                excelPath = 0
        End Select
    End Sub

    Sub ExcelInit()
        Dim MsgResult As MsgBoxResult = MsgBox("即將初始化系統及資料庫，請確保所有excel相關檔案皆儲存完畢並關閉，以免在初始化的過程中造成資料遺失。" & vbNewLine & "若已經準備好初始化的動作，請按確認鍵。", MsgBoxStyle.OkCancel Or MsgBoxStyle.Exclamation, "送出請求")
        If (MsgResult = MsgBoxResult.Ok) Then
            Try
                Dim myProcess As Process = System.Diagnostics.Process.Start("SystemInit.exe")
            Catch ex As Exception
                Dim MsgResult2 As MsgBoxResult = MsgBox("初始化失敗。請確認" & CurDir() & "\SystemInit.exe位置正確。")
                ems_login.Close()
            End Try
        Else
            ems_login.Close()
        End If
    End Sub
End Module
