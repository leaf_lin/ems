﻿Module ems_tmp_module
    Public Structure Type_basic
        Public Id As String
        Public Name As String
        Public Phone As String
    End Structure
    Public Data_basic As Type_basic

    Public Structure Type_unit
        Public Unit As String
        Public UnitId As String
    End Structure
    Public Data_unit As Type_unit

    Public Structure Type_item
        Public Id() As String
        Public Name() As String
        Public Return_reco() As Integer
        Public LR_num As Integer
        Public day As String
        Public admin As String
    End Structure
    Public Data_item As Type_item

    Sub TmpInit(ByVal admin As Integer, ByVal unit As Integer, ByVal basic As Integer)
        If admin = 1 Then
            Data_item.admin = ""
        End If
        If unit = 1 Then
            Data_unit.Unit = ""
            Data_unit.UnitId = ""
        End If
        If basic = 1 Then
            Data_basic.Id = ""
            Data_basic.Name = ""
            Data_basic.Phone = ""
            Data_unit.Unit = ""
        End If
    End Sub
End Module
