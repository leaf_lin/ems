﻿Public Class ems_itemlend
    '#######################System效果##########################
    Private Sub ems_itemlend_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        InputInit_func(0, 0, 1, 0)
        ExcelRead("\dat\器材清單.xls")
    End Sub

    Private Sub ems_itemlend_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        TmpInit(0, 1, 1)
        ems_mode.Show()
    End Sub

    '#######################Object##########################
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        For i = 2 To Excel_Cellvar
            If TextBox1.Text = Excel_itemlist(i, 0) And TextBox1.Text <> Nothing Then
                If Excel_itemlist(i, 5) = "P" Then
                    MsgBox("該器材無法外借。", , "提醒")
                ElseIf Excel_itemlist(i, 4) <= 0 Then
                    MsgBox("器材數量不足，請重新選擇。", , "提醒")
                ElseIf Excel_itemlist(i, 5) <> "P" And Excel_itemlist(i, 4) > 0 Then
                    '檢查輸入者是否有足夠的量借出 & 能否外借
                    ListBox1.Items.Add(Excel_itemlist(i, 2))
                    Excel_itemlist(i, 4) = Excel_itemlist(i, 4) - 1
                End If
            End If
        Next
    End Sub

    Private Sub 確認_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 確認.Click
        If DateTimePicker1.Text >= Format(Now(), "D") Then
            Dim MsgResult As MsgBoxResult = MsgBox("確定要送出嗎?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, "送出請求")
            If (MsgResult = MsgBoxResult.Yes) Then
                Data_item.LR_num = ListBox1.Items.Count - 1
                ReDim Data_item.Id(Data_item.LR_num)
                ReDim Data_item.Name(Data_item.LR_num)
                '將ListBox中的值全複製到Data_item.Name陣列中
                ListBox1.Items.CopyTo(Data_item.Name, 0)
                For j = 0 To Data_item.LR_num '陣列索引從零開始算
                    For i = 2 To Excel_Cellvar
                        If Data_item.Name(j) = Excel_itemlist(i, 2) Then
                            Data_item.Id(j) = Excel_itemlist(i, 0)
                        End If
                    Next
                Next
                Data_item.day = DateTimePicker1.Text
                Try
                    ExcelWrite("\dat\器材清單.xls")
                    ExcelWrite("\dat\器材借用紀錄.xls")
                    MsgBox("恭喜器材借用成功。", , "提醒")
                    InputInit_func(1, 1, 1, 0)
                Catch
                    MsgBox("系統錯誤，器材借用失敗。", , "提醒")
                End Try
            End If
        Else
            MsgBox("預計歸還時間錯誤，請重新選擇。", , "提醒")
        End If
    End Sub

    Private Sub 取消_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 取消.Click
        '初始化器材剩餘數量,日期,輸入欄
        InputInit_func(1, 1, 1, 1)
    End Sub

    Private Sub Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clear.Click
        InputInit_func(1, 0, 0, 0)
    End Sub

    '#######################ToolStripMebuItem##########################
    Private Sub 移除ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 移除ToolStripMenuItem.Click
        If ListBox1.SelectedIndex >= 0 Then
            '尋找被選取的項目的剩餘數量
            For i = 2 To Excel_Cellvar
                If ListBox1.SelectedItem = Excel_itemlist(i, 2) Then
                    Excel_itemlist(i, 4) = Excel_itemlist(i, 4) + 1
                End If
                i = i + 1
            Next
            ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
        Else
            MsgBox("請先選擇要刪除的項目名稱。", , "提醒")
        End If
    End Sub

    Private Sub 移除全部ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 移除全部ToolStripMenuItem.Click
        '初始化剩餘數量，並清空ListBox
        InputInit_func(0, 1, 0, 1)
    End Sub

    Private Sub 剩餘數量ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 剩餘數量ToolStripMenuItem.Click
        If ListBox1.SelectedIndex >= 0 Then
            For i = 2 To Excel_Cellvar
                If ListBox1.SelectedItem = Excel_itemlist(i, 2) Then
                    MsgBox("剩餘數量 : " & Excel_itemlist(i, 4), , "剩餘數量")
                End If
            Next
        Else
            MsgBox("請先選擇要查看的項目名稱。", , "提醒")
        End If
    End Sub

    Private Sub 詳細資料ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 詳細資料ToolStripMenuItem.Click
        If ListBox1.SelectedIndex >= 0 Then
            For i = 2 To Excel_Cellvar
                If ListBox1.SelectedItem = Excel_itemlist(i, 2) Then
                    MsgBox("編號 : " & Excel_itemlist(i, 0) & vbNewLine & "種類 : " & Excel_itemlist(i, 1) & vbNewLine & "名稱 : " & Excel_itemlist(i, 2) & vbNewLine & "備註 : " & Excel_itemlist(i, 6), , "關於")
                End If
            Next
        Else
            MsgBox("請先選擇要查看的項目名稱。", , "提醒")
        End If
    End Sub

    '#################自訂函數#####################
    Sub InputInit_func(ByVal TextBox As Integer, ByVal ListBox As Integer, ByVal DataTimePicker As Integer, ByVal ExcelLastnum As Integer)
        If TextBox = 1 Then
            TextBox1.Text = ""
        End If
        If ListBox = 1 Then
            ListBox1.Items.Clear()
        End If
        If DataTimePicker = 1 Then
            DateTimePicker1.Text = Format(Now(), "D")
        End If
        If ExcelLastnum = 1 Then
            For i = 2 To Excel_Cellvar
                Excel_itemlist(i, 4) = oXLSheet.Cells(i, 5).Value
            Next
        End If
    End Sub
End Class